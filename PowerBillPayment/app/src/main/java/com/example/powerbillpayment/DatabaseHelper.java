package com.example.powerbillpayment;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper  {
    public static final String DATABASE_NAME = "users.db";
    public static final String TABLE_NAME = "user_table";
    public static final String FN = "FIRST_NAME";
    public static final String LN = "LAST_NAME";
    public static final String UN = "USER_NAME";
    public static final String PASS = "PASSWORD";
    public static final String CITY = "CITY";
    public static final String HC = "HOUSECODE";
    public static final String BC = "BILLCODE";
    public static final String AN = "ACCOUNTNUMBER";

    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + "(FIRST_NAME TEXT, LAST_NAME TEXT, USER_NAME TEXT, PASSWORD TEXT, CITY TEXT, HOUSECODE TEXT, BILLCODE TEXT, ACCOUNTNUMBER TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(String fname, String lname, String uname, String pass, String city, String houseCode, String billCode, String accountNumber){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FN,fname);
        contentValues.put(LN,lname);
        contentValues.put(UN,uname);
        contentValues.put(PASS,pass);
        contentValues.put(CITY,city);
        contentValues.put(HC,houseCode);
        contentValues.put(BC,billCode);
        contentValues.put(AN,accountNumber);
        long result = db.insert(TABLE_NAME,null,contentValues);
        if (result == -1){
            return false;
        }
        else {
            return true;
        }
    }

    public Cursor getAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME,null);
        return res;
    }

    public Cursor authentication(String userName){
        SQLiteDatabase db = this.getWritableDatabase();
        String[] projections = {PASS};
        String selection = UN + " like ?";
        String[] selection_args = {userName};
        Cursor res = db.query(TABLE_NAME,projections,selection,selection_args,null,null,null);
        return res;
    }
}
