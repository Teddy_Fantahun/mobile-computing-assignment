package com.example.powerbillpayment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class Home extends AppCompatActivity {
    Button button;
    TextView logOut, loggedUserName, fee;
    Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        button = findViewById(R.id.pay);
        logOut = findViewById(R.id.logOut);
        loggedUserName = findViewById(R.id.home_UserName);
        fee = findViewById(R.id.monthlyFee);

        String name = LoggedUserModel.firstName + " " + LoggedUserModel.lastName;
        loggedUserName.setText(name);

        int br = random.nextInt(1001);
        int cent = random.nextInt(100);
        String monthlyFee = br + "." + cent + " ETH BIRR";
        fee.setText(monthlyFee);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setVisibility(View.INVISIBLE);
                Toast.makeText(Home.this, "Payment performed successfully!", Toast.LENGTH_LONG).show();
            }
        });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setVisibility(View.GONE);
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });
    }
}
