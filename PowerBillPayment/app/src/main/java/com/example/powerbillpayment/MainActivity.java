package com.example.powerbillpayment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView tv;
    EditText un, pass;
    Button login;
    DatabaseHelper myDb = new DatabaseHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = findViewById(R.id.tv_SignUp);
        un = findViewById(R.id.et_UserName);
        pass = findViewById(R.id.et_Password);
        login = findViewById(R.id.btn_SignIn);

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),SignUpActivity.class));
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!areFieldsEmpty()){
                    if(authentication(un.getText().toString(), pass.getText().toString())){
                        startActivity(new Intent(getApplicationContext(),Home.class));
                    }
                    else{
                        Toast.makeText(MainActivity.this, "User name or password is incorrect!", Toast.LENGTH_LONG).show();
                    }

                }
                else{
                    Toast.makeText(MainActivity.this, "Fields can not be empty!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public boolean areFieldsEmpty(){
        boolean result = false; // account number is not empty
        if( un.getText().toString().equals("") || pass.getText().toString().equals("") ){
            result = true;
        }
        return result;
    }

    public boolean authentication(String userName, String password){
        boolean result = false;

        /*
        Cursor cursor = myDb.authentication(un.getText().toString());
        if(cursor.isNull(0)){
            result = false;
        }
        else{
            while (cursor.moveToNext()) {
                if(cursor.getString(0).equals(pass.getText().toString())){
                    result = true;
                    break;
                }
            }
        }
         */

        Cursor res = myDb.getAllData();
        while(res.moveToNext()) {
            LoggedUserModel.firstName = res.getString(0);
            LoggedUserModel.lastName = res.getString(1);
            if(res.getString(2).equals(userName)  && res.getString(3).equals(password)){
                result = true;
                break;
            }
        }


        return result;
    }

}
