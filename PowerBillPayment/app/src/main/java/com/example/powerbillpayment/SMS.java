package com.example.powerbillpayment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.Random;

public class SMS extends AppCompatActivity {
    Button button;
    EditText code;
    Random random = new Random();
    final int random4DigitNumber = random.nextInt(10000);
    DatabaseHelper myDb = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_m_s);

        button = findViewById(R.id.finalVerification);
        code = findViewById(R.id.code);

       // String phoneNo = "251988535500";
       // String number = phoneNo.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
       // String formattedNumber = PhoneNumberUtils.formatNumber(phoneNo);
      //  String sms = "0000";


        Toast.makeText(getApplicationContext(), "Code : " + random4DigitNumber, Toast.LENGTH_LONG).show();

        /*
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(formattedNumber, null, sms, null, null);

         //   Toast.makeText(getApplicationContext(), "SMS Sent!", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
         //   Toast.makeText(getApplicationContext(), "SMS failed, please try again later!", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

         */

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isCodeEmpty() == false){
                    int inputCode = Integer.parseInt(code.getText().toString());
                    if(inputCode == random4DigitNumber){
                        insertDataIntoDatabase();
                    }
                    else{
                        Toast.makeText(SMS.this, "Code is incorrect!", Toast.LENGTH_LONG).show();
                    }

                }
                else{
                    Toast.makeText(SMS.this, "Field can not be empty!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    public boolean isCodeEmpty(){
        boolean result = false; // account number is not empty
        if(code.getText().toString().equals("")){
            result = true;
        }
        return result;
    }

    public void insertDataIntoDatabase(){
        boolean isInserted = myDb.insertData(DataModel.firstName,DataModel.lastName,DataModel.userName,DataModel.password,
                DataModel.city,DataModel.houseCode,DataModel.billCode,DataModel.accountNumber);
        if(isInserted == true){
            Toast.makeText(SMS.this, "You Registered Successfully!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(),Home.class));
        }
        else{
            Toast.makeText(SMS.this, "Registration Failed!", Toast.LENGTH_SHORT).show();
        }
    }

}
