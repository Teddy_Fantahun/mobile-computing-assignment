package com.example.powerbillpayment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SignUpActivity extends AppCompatActivity {
    Button button;
    EditText fn, ln, un, pass1, pass2, city, houseCode, billCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        button = findViewById(R.id.btn_Form_1);
        fn = findViewById(R.id.fn);
        ln = findViewById(R.id.ln);
        un = findViewById(R.id.form_UserName);
        pass1 = findViewById(R.id.form_Password1);
        pass2 = findViewById(R.id.form_Password2);
        city = findViewById(R.id.form_City);
        houseCode = findViewById(R.id.form_HouseCode);
        billCode = findViewById(R.id.form_BillCode);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFormFilled() == true) {
                    if(checkPasswordMatch() == true){
                        commitUserData();
                        commitUserAccountInformation();
                        startActivity(new Intent(getApplicationContext(), Verification.class));
                    }
                    else{
                        Toast.makeText(SignUpActivity.this, "Passwords doesn't match!", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(SignUpActivity.this, "One or more fields empty!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

        public boolean isFormFilled () {
            boolean result = true;
            if (fn.getText().toString().equals("") || ln.getText().toString().equals("") || un.getText().toString().equals("") || pass1.getText().toString().equals("") ||
                    pass2.getText().toString().equals("") || city.getText().toString().equals("") || houseCode.getText().toString().equals("") ||
                    billCode.getText().toString().equals("")) {
                result = false;
            }

            return result;
        }

        public boolean checkPasswordMatch () {
        boolean result = true;
        if (pass1.getText().toString().equals(pass2.getText().toString()) == false) {
                result = false;
            }
        return result;
        }

        public void commitUserData(){
        DataModel.firstName = fn.getText().toString();
        DataModel.lastName = ln.getText().toString();
        DataModel.userName = un.getText().toString();
        DataModel.password = pass1.getText().toString();
        DataModel.city = city.getText().toString();
        DataModel.houseCode = houseCode.getText().toString();
        DataModel.billCode = billCode.getText().toString();
        }

        public void commitUserAccountInformation(){
        LoggedUserModel.firstName = fn.getText().toString();
        LoggedUserModel.lastName = ln.getText().toString();
        }

}

