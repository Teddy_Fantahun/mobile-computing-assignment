package com.example.powerbillpayment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.Toast;

public class Verification extends AppCompatActivity {
    Button button;
    EditText account;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        button = findViewById(R.id.btn_Verify);
        account = findViewById(R.id.accountNumber);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isAccountNumberEmpty() == false){
                    commitAccountNumber();
                    startActivity(new Intent(getApplicationContext(),SMS.class));
                }
                else{
                    Toast.makeText(Verification.this, "Field can not be empty!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    public boolean isAccountNumberEmpty(){
        boolean result = false; // account number is not empty
        if(account.getText().toString().equals("")){
            result = true;
        }
        return result;
    }

    public void commitAccountNumber(){
        DataModel.accountNumber = account.getText().toString();
    }
}
